// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    building:'',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName'), // 如需尝试获取用户信息可改为false
    loupanCollect:{
    Id:{},
    isCollected:{}
    },
  },
  // 事件处理函数
  psvNavigate:function(){
    wx.navigateTo({
      url: '../../pages/psv/psv',
    })
  },
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  viewer:function(){ 
    wx.navigateTo({
      url: '../../pages/test_view/test_view',
    })
  },
  onLoad:function(){
    var that = this;
    wx.request({
      url: 'http://112.124.106.13:8000/prod-api/loupan-state?state=3',
      dataType: 'JSON',
      header:  {'content-type':'application/json'},
      method: 'GET',
      success: (res) => {
        console.log(res.data);
        var array = JSON.parse(res.data);
        that.setData({
          building:array
        });
      },
      fail:(err) =>{
        console.log(failed);
      }
    })
   
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  handleCollect(e) {
    var that = this;
    console.log(e.currentTarget.dataset.id);
    var loupanId = e.currentTarget.dataset.id;
    wx.request({
      url: 'http://112.124.106.13:8000/prod-api/collection',
      data:{
        id:0,
        user:app.globalData.userId,
        loupanCollect:loupanId,
      },
      fail: (err) => {
        console.log("failed");
      },
      header:{"Content-type":"application/x-www-form-urlencoded"},
      method:"POST",
      success: (res) => {
        console.log("successed");
        this.data.loupanCollect["Id"].append(loupanId)
        this.data.loupanCollect["isCollected"].append(true)
      },
    })
    wx.setStorageSync('id', that.data.loupanId)

},

})
