// pages/idtState/idtState.js
var app = getApp();

  
Page({

  /**
   * 页面的初始数据
   */ 
  data: {
    user:{},
    build:{},
    host:{},
    userId:'',
    active:0,
    steps:[
      {
        text: '步骤一',
        desc: '提交信息',
        inactiveIcon: 'location-o',
        activeIcon: 'success',
      },
      {
        text: '步骤二',
        desc: '后台审核',
        inactiveIcon: 'location-o',
        activeIcon: 'plus',
      },
      {
        text: '步骤三',
        desc: '审核完成',
        inactiveIcon: 'location-o',
        activeIcon: 'cross',
      },
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    var user = app.globalData.user;
    var host = app.globalData.host;
    that.setData({
      build:app.globalData.build,
    })
    var userId = app.globalData.userId;
    wx.request({
      url: 'http://112.124.106.13:8000/prod-api/order-user?userId='+userId,
      header:{'content-type':'application/json'},
      method: 'GET',
      data:{
      },
      fail:(err) =>{
        console.log("failed");
      },
      success: (res) => {
        console.log(res.data);
        that.setData({
          active:(res.data[0].state)-1,
          user:user,
          host:host,
        })
      app.globalData.state = that.data.active
      console.log(res.data[0].state);
      },
    })
    console.log(that.data.active);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})