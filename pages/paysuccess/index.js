// pages/paysuccess/index.js
var app = getApp();

  
Page({

    /**
     * 页面的初始数据
     */
    data: {
        host:{
            hostName:'',
            host_idt:'',
            room:[],
            houseSrc:[],
          },
          userId:'',
          renzheng:false,
          userInfo:{},
          carport:{},
          build:{
            name:'',
            location:'',
            louceng:'',
            price:'',
            photo:'',
          },
          user:{
            user_name:'',
            idt_num:'',
            tel_num:'',
            idt_card:'',
            state:0,
          },
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        that.setData({
            build:app.globalData.build,
            user:app.globalData.user
        })
    },

    

    end: function(){
        wx.navigateTo({
            url:'/pages/end/end',
          })
    },
    index: function(){
        wx.navigateTo({
            url:'../../pages/order/logs',
          })
    },
})