// pages/host/host.js
var app = getApp();

  
Page({

  /**
   * 页面的初始数据
   */
  data: {
    usr:[{
      usrName:'',usrId:'',usrTel:'',
    },],
    columns:'',
    error:false,
    err_usrName:false,
    err_idt_num:false,
    err_tel_num:false,
    err_hostName:false,
    err_room_num:false,
    err_host_idt:false,
    source:' ',
    source2:'',
    user_name:'',
    idt_num:'',
    tel_num:'',
    room:[],
    location:'',
    loc:'',
    louceng:'',
    title:'',
    idURL:'',
    hostName:'',
    host_idt:'',
    show: false,
    multiArray: [['1号楼', '2号楼','3号楼'], ['一楼', '二楼', '三楼', '四楼', '五楼','六楼'], ['1101', '1102','1103']],
    multiIndex: [0, 0, 0],
    index:0,
    index2:0,
    tempFilePaths: [],
    tempFilePaths2:[],
    carport:[],
    access_token:'24.bbc8f2cdd6196313160650ac697e47e6.2592000.1632644008.282335-24761479',
    tempName:'',
    tempNum:'',
    idtSrc:'',
    houseSrc:[],
    userId:'',
    ad_zhuang:'',
    ad_danyuan:'',
    ad_fang:'',
    plot:'',
    xiaoqu:'',
    user:{
      user_name:'',
      idt_num:'',
      tel_num:'',
      idt_card:'',
      state:1,
    },
    columns:[],
    radio:'',
    intendCarports:'',
    intendCarpoetsId:'',
    tempName:'',
    tempIdt:'',
    currentCarport:'',
    currentCarportId:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      user:app.globalData.user
    });
    var title = options.title;
    var loc = options.loc;
    var louceng = options.louceng;
    var userId = options.userId;
    var tempName = options.tempName;
    var tempIdt = options.tempIdt;
    console.log(userId);
    this.setData({
      title:title,
      loc:loc,
      louceng:louceng,
      userId:userId,
      tempName:tempName,
      tempIdt:tempIdt,
        });
        console.log(this.data.userId);
        console.log(that.data.tempName);
        console.log(that.data.tempIdt);
        wx.request({
          url: 'http://112.124.106.13:8000/prod-api/loupan',
          dataType:"JSON",
          header:{"Content-type":"application/json"},
          method: 'GET',
          success: (res) => {
            var array = JSON.parse(res.data);
            for(var i = 0;i<array.length;i++){
              that.setData({
                ["columns["+i+"]"]:array[i].name,
              })
            }
            console.log(that.data.columns);
          },
        })
  },
  onChange:function(event){
    var that = this;
    that.setData({
      radio:event.detail,
    })
    console.log(that.data.radio);
    var tempName = that.data.tempName;
    var tempIdt = that.data.tempIdt;
    if(that.data.radio==1){
      that.setData({
        hostName:tempName,
        host_idt:tempIdt,
      })
    }
  },
  /*上传房产证*/
  uploadImage2:function(){
    var that = this;
    wx.chooseImage({
      count: 9, // 默认最多9张图片，可自行更改
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: (res) => {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths;
        that.setData({
          tempFilePaths2: tempFilePaths
        })
        console.log("Choose imgae success!");
        console.log(tempFilePaths);
        var url = 'http://112.124.106.13:8000/prod-api/post-photo'
        var successUp = 0;
        var failUp = 0;
        var count = 0;
        var length = tempFilePaths.length;
        var imagePaths = that.data.tempFilePaths2;
        that.upLoadOneByOne(url,imagePaths,successUp,failUp,count,length);
          console.log(this.data.houseSrc);
      },
    })
    
  },
  PreviewImg2: function (e) {

    let index = e.target.dataset.index;

    let that = this;

    //console.log(that.data.tempFilePaths[index]);

    //console.log(that.data.tempFilePaths);

    wx.previewImage({

      current: that.data.tempFilePaths2[index],

      urls: that.data.tempFilePaths2,

    })
  },
  DeleteImg2: function (e) {
    var that = this;
    var tempFilePaths2 = that.data.tempFilePaths2;
    var index = e.currentTarget.dataset.index;//获取当前长按图片下标
    wx.showModal({
      title: '提示',
      content: '确定要删除此图片吗？',
      success: function (res) {
        if (res.confirm) {
          //console.log('点击确定了');
          tempFilePaths2.splice(index, 1);
        } else if (res.cancel) {
          //console.log('点击取消了');
          return false;
        }
        that.setData({
          tempFilePaths2
        });
      }
    })
  },

  upLoadOneByOne(url,imagePaths,successUp,failUp,count,length){
    var that = this;
    wx.uploadFile({
      url: url,
      filePath: imagePaths[count],
      name: 'file',
      fail: (err) => {
        console.log("failed");
        failUp++;
      },
      success: (res) => {
        successUp++;
       console.log(res.data);
       this.setData({
         ['houseSrc['+count+']']:res.data,
       });
       console.log(that.data.houseSrc);
      },
      complete:function(e){
        count++;
        if(count == length ){
          wx.showToast({
            title: '上传成功！',
            icon:'icons/duigou.png',
          })
        }
        else{
          that.upLoadOneByOne(url,imagePaths,successUp,failUp,count,length)
        }
      },

    })

  },
  hostRequest:function(){
    var that = this;
    var userId = app.globalData.userId
    var ad_zhuang = (that.data.room[0]+1);
    var ad_danyuan =  (that.data.room[1]+1);
    var ad_fang =  (that.data.room[2]+1);
    app.globalData.host.hostName = that.data.hostName;
    app.globalData.host.host_idt = that.data.host_idt;
    app.globalData.host.room[0] = ad_danyuan;
    app.globalData.host.room[1] = ad_zhuang;
    app.globalData.host.room[2] = ad_fang;
    app.globalData.host.houseSrc = that.data.houseSrc;
    //app.globalData.host赋值完毕
    that.setData({
      ad_danyuan:ad_danyuan,
      ad_zhuang:ad_zhuang,
      ad_fang:ad_fang,
    })
    console.log(that.data.user.state);
    console.log(that.data.user.idt_card);
    wx.request({
      url: 'http://112.124.106.13:8000/prod-api/order',
      data: {
        id:0,
        user:that.data.userId,
        state:that.data.user.state,
        ad_zhuang:that.data.ad_zhuang,
        ad_danyuan:that.data.ad_danyuan,
        ad_fang:that.data.ad_danyuan,
        benren:1,
        shenfenphoto:JSON.stringify(app.globalData.user.idt_card),
        hukou:that.data.houseSrc,
        carport:that.data.currentCarportId,
      },
      fail: (err) => {
        console.log("failed");
      },
      header: { "Content-Type": "application/x-www-form-urlencoded"},
      method: 'POST',
      success: (res) => {
        console.log("success");
        wx.request({
          url: 'http://112.124.106.13:8000/prod-api/order-user?userId='+userId,
          header: {"Content-type":"application/json"},
          method: 'GET',
          success: (res) => {
            console.log(res.data);
            console.log(res.data[0].state);
            app.globalData.orderId=res.data[0].id;
            console.log(app.globalData);
            console.log(that.data.userId);
          },
        })
      },
    })
    wx.showToast({
      title: '提交成功',
    })
    app.globalData.renzheng = true
    setTimeout(() => {
      wx.navigateTo({
        url: '../../pages/audit/audit',
      })
    }, 3000);
    
 
  },
   /*选择小区*/
  bindPickerChange:function(event){
    var that = this;
    this.setData({
      index:event.detail.value,
      xiaoqu:this.data.columns[event.detail.value]
    })
    console.log(this.data.xiaoqu);
    that.setData({
      intendCarports:'',
      intendCarportsId:'',
    })
    wx.request({
      url: 'http://112.124.106.13:8000/prod-api/carport-loupan?loupan='+that.data.xiaoqu,
      dataType: 'JSON',
      fail: (err) => {
        console.log("failed");
      },
      header:{"Content-type":"application/json"},
      method: 'GET',
      success: (res) => {
        console.log(res.data);
        var array = JSON.parse(res.data);
        for(var i = 0;i<array.length;i++){
          that.setData({
            ["intendCarports["+i+"]"]:array[i].number,
            ["intendCarportsId["+i+"]"]:array[i].id
          })
        }
        console.log(that.data.columns);
        console.log(that.data.xiaoqu);
        console.log(that.data.intendCarports);
        console.log(that.data.intendCarpoetsId);
      },
    })
  },
  /*意向车位*/
  bindPickerChange2:function(event){
    var that = this;
    var index2 = event.detail.value;
    that.setData({
      index2:event.detail.value,
     });
     var intendCarports = that.data.intendCarports;
     var index2 = that.data.index2;
     var intendCarportsId = that.data.intendCarportsId;
    that.setData({
      currentCarport:intendCarports[index2],
      currentCarportId:intendCarportsId[index2]
    })
    app.globalData.user.intendCarportsId = that.data.currentCarportId;
    console.log(app.globalData.user.intendCarportsId);
    console.log(index2);
    console.log(that.data.intendCarportsId[index2]);
    console.log(that.data.currentCarportId);
  },
  /*房间选择*/
  bindMultiPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      multiIndex: e.detail.value,
      room:e.detail.value
    })
    console.log("楼号"+(this.data.room[0]+1));
  },
  bindMultiPickerColumnChange: function (e) {
    console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    var data = {
      multiArray: this.data.multiArray,
      multiIndex: this.data.multiIndex
    };
    data.multiIndex[e.detail.column] = e.detail.value;
    console.log(e.detail.column);
    switch (e.detail.column) {
      case 0:
        switch (data.multiIndex[0]) {
          case 0:
            data.multiArray[1] = ['一楼', '二楼', '三楼', '四楼', '五楼','六楼'];
            data.multiArray[2] = ['1101', '1102','1103'];
            break;
          case 1:
            data.multiArray[1] = ['一楼', '二楼', '三楼', '四楼', '五楼','六楼'];
            data.multiArray[2] = ['2101', '2102','2103'];
            break;
          case 2:
            data.multiArray[1] = ['一楼', '二楼', '三楼', '四楼', '五楼','六楼'];
            data.multiArray[2] = ['3101', '3102','3103'];
            break;     
        }
        data.multiIndex[1] = 0;
        data.multiIndex[2] = 0;
        break;
      case 1:
        switch (data.multiIndex[0]) {
          case 0:
            switch (data.multiIndex[1]) {
              case 0:
                data.multiArray[2] = ['1101', '1102','1103'];
                break;
              case 1:
                data.multiArray[2] = ['1201','1202','1203'];
                break;
              case 2:
                data.multiArray[2] = ['1301','1302','1303'];
                break;
              case 3:
                data.multiArray[2] = ['1401','1402','1403'];
                break;
              case 4:
                data.multiArray[2] = ['1501','1502','1503'];
                break;
              case 5:
                data.multiArray[2] = ['1601','1602','1603'];
                break;
            }
            break;
            case 1:
            switch (data.multiIndex[1]) {
              case 0:
                data.multiArray[2] = ['2101', '2102','2103'];
                break;
              case 1:
                data.multiArray[2] = ['2201','2202','2203'];
                break;
              case 2:
                data.multiArray[2] = ['2301','2302','2303'];
                break;
              case 3:
                data.multiArray[2] = ['2401','2402','2403'];
                break;
              case 4:
                data.multiArray[2] = ['2501','2502','2503'];
                break;
              case 5:
                data.multiArray[2] = ['2601','2602','2603'];
                break;
            }
            break;
            case 2:
            switch (data.multiIndex[1]) {
              case 0:
                data.multiArray[2] = ['3101', '3102','3103'];
                break;
              case 1:
                data.multiArray[2] = ['3201','3202','3203'];
                break;
              case 2:
                data.multiArray[2] = ['3301','3302','3303'];
                break;
              case 3:
                data.multiArray[2] = ['3401','3402','3403'];
                break;
              case 4:
                data.multiArray[2] = ['3501','3502','3503'];
                break;
              case 5:
                data.multiArray[2] = ['3601','3602','3603'];
                break;
            }
            break;
        }
        data.multiIndex[2] = 0;
        console.log(data.multiIndex);
        break;
    }
    this.setData(data);
  },
  hostNameInput:function(event){
    this.setData({
      hostName:event.detail
    })
  },
  xiaoquInput:function(event){
    this.setData({
      xiaoqu:event.detail
    })
  },
  host_idtInput:function(event){
    this.setData({
      host_idt:event.detail
    })
  },
  plotInput:function(event){
    this.setData({
        plot:event.detail
    })
  },
  onConfirm:function(event){
    
  },
})