
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
      carportMay:'',
      userInfo:{},
      carport:{},
      build:{
        name:'',
        location:'',
        louceng:'',
        price:'',
        photo:'',
      },
      user:{
        user_name:'',
        idt_num:'',
        tel_num:'',
        idt_card:'',
        room:[],
        house:[],
      },
      state:1
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      var that = this;
  var carport =  app.globalData.carport;
  var build = app.globalData.build;
  var user = app.globalData.user;
  var host = app.globalData.host;
  var userId = app.globalData.userId;
  that.setData({
    carport:carport,
    build:build,
    user:user,
    host:host,
    userId:userId,
  });
  wx.request({
    url: 'http://112.124.106.13:8000/prod-api/order-state?state=1',
    fail: (err) => {
      console.log("failed");
    },
    header:{"content-type":"application/json"},
    method:'GET',
    success: (res) => {
      console.log(res.data);
      this.setData({
        carportMay:res.data,
      })
    },
  })
  console.log(that.data.state);
    },
    onChange:function(event){
      var that = this;
      var state = event.detail.name;
      that.setData({
        state:state,
      })
      console.log(that.data.state);
      wx.request({
        url: 'http://112.124.106.13:8000/prod-api/order-state?state='+state,
        fail: (err) => {
          console.log("failed");
        },
        header:{"content-type":"application/json"},
        method:'GET',
        success: (res) => {
          this.setData({
            carportMay:res.data,
          })
        },
      })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
 
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    onTapDayWeather(){
        wx.navigateTo({url:'/pages/pay/pay'})
      },
    onContructlog(){
      wx.navigateTo({url:'/pages/contructlog/index'})
    },
    onFullsuccess(){
      wx.navigateTo({url:'/pages/agency/index'})
    },
    onAudit(){
      wx.navigateTo({url:'/pages/audit/audit'})
    },
})