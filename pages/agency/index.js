// pages/agency/index.js
var app = getApp();

  
Page({
  data: {
    checked: false,
    use:"车库",
    Area:"",
    ddl:"2073.10.28",
    discount:"-0",
    carport:{},
    build:{
      name:'',
      location:'',
      louceng:'',
      price:'',
    },
    user:{
      user_name:'',
      idt_num:'',
      tel_num:'',
      idt_card:'',
      room:[],
      house:'',
    },
  },

  onChange(event) {
    this.setData({
      checked: event.detail,
    });
  },
  viewer:function(){
    var that = this;
    var carportId = app.globalData.carport.id;
    wx.navigateTo({
      url: '../../pages/psv/psv?carportId='+carportId,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      carport:app.globalData.carport,
      build:app.globalData.build,
      user:app.globalData.user,
    })
    console.log(that.data.carport);
    console.log(that.data.build);
    console.log(that.data.user);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onFullsuccess(){
    wx.navigateTo({url:'/pages/fullmoney/index'})
  }
})