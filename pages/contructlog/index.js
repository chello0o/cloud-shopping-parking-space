// pages/contructlog/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        steps: [
            {
              text: '派送中',
              desc: '【绍兴市】（0575-86253898）业务员董凯杰正在派件',
            },
            {
              text: '运输中',
              desc: '【绍兴市】快件已到达【绍兴中转部】',
            },
            {
              text: '已揽收',
              desc: '【四川西昌】（0834-3205958）的刘光武（15760232485）已揽收',
            },
            {
              text: '待发货',
              desc: '您已签订合同，正在打印中',
            },
          ],
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})