// pages/name/name.js
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    usr:[{
      usrName:'',usrId:'',usrTel:'',
    },],
    error:false,
    err_usrName:false,
    err_idt_num:false,
    err_tel_num:false,
    err_hostName:false,
    err_room_num:false,
    err_host_idt:false,
    source:' ',
    source2:'',
    user_name:'',
    idt_num:'',
    tel_num:'',
    hostName:'',
    room:[],
    location:'',
    loc:'',
    louceng:'',
    title:'',
    idURL:'',
    host_idt:'',
    show: false,
    multiArray: [['1号楼', '2号楼','3号楼'], ['一楼', '二楼', '三楼', '四楼', '五楼','六楼'], ['1101', '1102','1103']],
    multiIndex: [0, 0, 0],
    tempFilePaths: [],
    tempFilePaths2:[],
    carport:[],
    access_token:'24.bbc8f2cdd6196313160650ac697e47e6.2592000.1632644008.282335-24761479',
    tempName:'',
    tempNum:'',
    idtSrc:'',
    houseSrc:[],
    renzheng:false,
    resultImage:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var carport = JSON.parse(options.carport);
    var title = options.title;
    var loc = options.loc;
    var louceng = options.louceng;
    this.setData({
      carport:carport,
      title:title,
      loc:loc,
      louceng:louceng,

    });
    console.log(carport);
  },

 /*上传图片，解码获取人像*/
  uploadImage:function(){
    let that = this;
    wx.chooseImage({
      count: 9, // 默认最多9张图片，可自行更改
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: (res) => {
        wx.showToast({
          title: '正在上传...',
          icon: 'loading',
          mask: true,
          duration: 1000
        })
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        let tempFilePath = res.tempFilePaths;
        that.setData({
          tempFilePaths: tempFilePath
        })
        var tempFilePaths = res.tempFilePaths[0]
        that.setData({
          idURL:tempFilePaths
        })
        console.log("Choose imgae success!");
        console.log(tempFilePaths);
        wx.uploadFile({
          filePath: this.data.idURL,
          name: 'file',
          url: "http://112.124.106.13:8000/prod-api/post-photo",
          fail: (err) => {
            console.log("Error!");
          },
          formdata:{
            file:"file"
          },
          header: {"Content-Type": "multipart/form-data" },
          success: (res) => {
            console.log("success");
            this.setData({
              idtSrc:res.data
            })
            console.log(this.data.idtSrc);
            app.globalData.idt_card = this.data.idtSrc    //获取身份证照片链接
          },
        })
        wx.getFileSystemManager().readFile({
          filePath:this.data.idURL,
          encoding:'base64',
          success:(res) =>{
            //console.log(res.data);  //图片Base64
            that.setData({
              base64:res.data
            });
            wx.request({
              url: 'https://aip.baidubce.com/rest/2.0/ocr/v1/idcard?access_token='+that.data.access_token,
              data: {
                image:that.data.base64,
                id_card_side:'front',
                image_type:'BASE64',
                detect_photo:'true'
              },
              header: {'content-type':'application/x-www-form-urlencoded'},
              method: 'POST',
              success: (res) => {          //OCR识别Success
                console.log(res.data);
                var array = res.data;
                console.log(res.data.words_result.姓名.words);
                that.setData({
                 user_name:res.data.words_result.姓名.words,
                 idt_num:res.data.words_result.公民身份号码.words,
                })
                console.log(res.data.photo);
                //console.log("头像base64:"+res.data.photo);
                var Base64 = res.data.photo;
                app.globalData.user.idtBase64 = Base64;
                 console.log(that.data.resultImage);
              },
              fail:(err) =>{
                console.log("failed");
              }
            })
          }
          
        })
            
      },
    })
  },
  /*照片预览*/
  PreviewImg: function (e) {

    let index = e.target.dataset.index;

    let that = this;

    //console.log(that.data.tempFilePaths[index]);

    //console.log(that.data.tempFilePaths);

    wx.previewImage({

      current: that.data.tempFilePaths[index],

      urls: that.data.tempFilePaths,

    })

  },
  /*删除图片*/
  DeleteImg: function (e) {
    var that = this;
    var tempFilePaths = that.data.tempFilePaths;
    var index = e.currentTarget.dataset.index;//获取当前长按图片下标
    wx.showModal({
      title: '提示',
      content: '确定要删除此图片吗？',
      success: function (res) {
        if (res.confirm) {
          //console.log('点击确定了');
          tempFilePaths.splice(index, 1);
        } else if (res.cancel) {
          //console.log('点击取消了');
          return false;
        }
        that.setData({
          tempFilePaths
        });
      }
    })
  },
  /*基础信息认证*/
  usrRequest:function(){
    var that = this;
    console.log(that.data.user_name);
    console.log(that.data.idt_num);
    app.globalData.user.user_name = that.data.user_name,
    app.globalData.user.idt_num = that.data.idt_num,
    app.globalData.user.tel_num = that.data.tel_num,
    console.log(app.globalData.user);
    wx.request({
      url: 'http://112.124.106.13:8000/prod-api/user',
      data:{
        name:that.data.user_name,
        telenumber:that.data.tel_num,
        shenfen:that.data.idt_num,
        shenfenphoto:that.data.idtSrc,
      },
      fail: (err) => {
        console.log("failed");
      },
      header: { "Content-Type": "application/x-www-form-urlencoded"
    },
      method: 'POST',
      success: (res) => {
        console.log("Basic infoamation success");
        console.log(that.data.idtSrc);
        app.globalData.user.idt_card = that.data.idtSrc;
      },
    })
    /*通过电话获取id*/
    var telenumber = that.data.tel_num
    wx.request({
      url: 'http://112.124.106.13:8000/prod-api/user-tel?tel='+telenumber,
      fail: (err) => {
        console.log("failed");
      },
      header: {'content-type':'application/json'},
      method: 'GET',
      success: (res) => {
        console.log(res.data);
        app.globalData.userId = res.data.id;
        var userId = res.data.id;
        this.setData({
          userId:userId
        })
        console.log(res.data);
        console.log(res.data.id);
        console.log(userId);
        wx.showToast({
          title: '实名成功！',
        }),
        setTimeout(() => {
          wx.navigateTo({
            url: '../../pages/host/host?userId='+that.data.userId+"&tempName="+that.data.user_name+"&tempIdt="+that.data.idt_num,
          })
        }, 3000);
      },
    })
   
  },
  usrNameInput:function(event){
    this.setData({
      user_name:event.detail,
    })
    },
    usrIdInput:function(event){
      this.setData({
        idt_num:event.detail
      });
    },
    usrTelInput:function(event){
      this.setData({
        tel_num:event.detail
      });
    },
})