// pages/collection/index.js
var app = getApp();

  
Page({

  /**
   * 页面的初始数据
   */
  data: {
    url : app.globalData.url,
    collection:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var url = that.data.url;
    wx.request({
      url: url+'/collection?tel=18634407274',
      dataType: 'JSON',
      fail: (err) => {
        console.log("failed");
      },
      header: {'content-type':'application/json'},
      method: 'GET',
      success: (res) => {
        console.log("success");
        var array = JSON.parse(res.data);
        that.setData({
          collection:array
        })
      },
    })
  },

})