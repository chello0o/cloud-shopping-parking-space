// pages/details_p/details_p.js
var app =  getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    name:'',
    location:'',
    price:'',
    carports:[],
    louceng:[],
    currentCarport:'',
    currentLouceng:'',
    currentBuilding:'',
    carport:'',
    touxiang:'',
    imgurl_loc:'https://i.loli.net/2021/08/04/iTbBuzL3yYmGeFq.jpg',
    imgUrls: [
      'https://wx2.sinaimg.cn/mw690/006qbn5yly1gtwjv9jsm8j30ly087gmb.jpg',
      'https://wx2.sinaimg.cn/mw690/006qbn5yly1gtwjvd9v0mj30l008g0t4.jpg',
      'https://wx4.sinaimg.cn/mw690/006qbn5yly1gtwjvbf6umj30k607ujsc.jpg'
    ],
    indicatorDots: true,  //是否显示面板指示点
    autoplay: true,      //是否自动切换
    interval: 3000,       //自动切换时间间隔
    duration: 1000,       //滑动动画时长
    inputShowed: false,
    inputVal: "",
    activeNames: ['1'],
    isChecked:false,
    show1:false,
    show2:false,
    show3:false,
    show4:false,
    show_louceng:false,
    show_idt:false,
    showLocked:false,
    showUnlocked:false,
    idt:false,
    locked:false,
    showShare: false,
    IsActive:false,
    options: [
      { name: '微信', icon: 'wechat', openType: 'share' },
      { name: '微博', icon: 'weibo' },
      { name: '复制链接', icon: 'link' },
      { name: '分享海报', icon: 'poster' },
      { name: '二维码', icon: 'qrcode' },
    ],
    columns: ['负一层',  '负二层',  '负三层'],
    button_state:false,
    isDisabled:false,
    text:"立即领取",
    btn:"btn1",
    currentTab:0,
    active:1,
    userInfo: {},
    hasUserInfo: false,
    canIUseGetUserProfile: false,
    renzheng:false,
    show_renzheng:false,
    show_renchou:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  /*锁定车位函数*/
  shiming:function(){
    wx.navigateTo({
      url: '../../pages/name/name',
    })
  },
  showRenzheng:function(){
    this.setData({
      show_renzheng:true
    })
  },
  showLocked:function(event){
    var that = this
   var value=event.currentTarget.dataset;
   var id=value.id;
   var text=value.text;
   console.log(value);
    this.setData({
      currentCarport:id,
      showLocked:true
    })
    
    console.log("state:"+that.data.carports[id].state);
    console.log(value);
  },
  showUnlocked:function(event){
    var that = this;
    var value = event.currentTarget.dataset;
    var id = value.id
    that.setData({
      currentCarport:id,
      showUnlocked:true
    });
  },
  isLocked:function(event){
    var that = this ;
    var id = that.data.currentCarport;
    this.setData({
      [`carports[${id}].state`]:3
     })
     wx.showToast({
      title: '选择成功！',
    })
  },
   /*解锁车位函数*/
  isUnlocked:function(event){
    var that = this ;
    var value = event.currentTarget.dataset;
    var id = that.data.currentCarport;
    console.log(id);
    this.setData({
      [`carports[${id}].state`]:1
    });
    wx.showToast({
      title: '解锁成功！',
    })
  },
  show_louceng:function(event){
    var value = event.currentTarget.dataset
    var louceng = value.louceng;
    this.setData({
      show_louceng:true,
      currentLouceng:louceng
    })
    console.log(this.data.currentLouceng);
    console.log(this.data.name);
    var name = this.data.name;
    var louceng = this.data.louceng
    app.globalData.build.louceng = this.data.currentLouceng;
    console.log(app.globalData.build);
    wx.request({
      url: 'http://112.124.106.13:8000/prod-api/carport-louceng?loupan='+this.data.name+'&louceng='+this.data.currentLouceng,
      dataType: 'JSON',
      fail: (err) => {
        console.log("failed");
      },
      header: {'content-type':'application/json'},
      method: 'GET',
      success: (res) => {
        var array = JSON.parse(res.data);
        console.log(array);
        this.setData({
          carports:array
        });
      },
    })
  },
   /*未锁定车位弹出框函数*/
  showDialog3:function(){
    this.setData({show3:true});
  },
  /*锁定车位弹出框*/
  showDialog4:function(){
    this.setData({show4:true});
  },
  onReady:function(){
    var userId = app.globalData.userId
    wx.request({
      url: 'http://112.124.106.13:8000/prod-api/order-user?userId='+userId,
      header: {"Content-type":"application/json"},
      method: 'GET',
      success: (res) => {
        app.globalData.user.state=res.data[0].state;
        console.log(res.data.state);
      },
    })
  },
   onLoad:function (options){

    console.log(options.data);
    var that = this; 
    var layer = options.louceng.split(",");
    if(app.globalData.user.state==2){
      app.globalData.renzheng = true;
    }
    else{
      app.globalData.renzheng = false;
    }
    that.setData({
      id:options.id,
      name:options.name,
      location:options.location,
      louceng:layer,
      price:options.price,
      renzheng:app.globalData.renzheng,
    });
    app.globalData.build.name = options.name;
    app.globalData.build.location = options.location;
    app.globalData.build.price = options.price;
    app.globalData.build.photo = options.photo;
    console.log(app.globalData.build);
    var name = that.data.name
   wx.request({
     url: 'http://112.124.106.13:8000/prod-api/carport-loupan?loupan='+name,
     dataType: 'JSON',
     fail: (err) => {
       console.log(error);
     },
     header:  {'content-type':'application/json'},
     method: 'GET',
     success: (res) => {
       console.log(res.data);
       var array = JSON.parse(res.data);
       console.log(that.data.louceng);
     },
   })
     var data = {
       "datas": [
         {
           "id": 1,
           "imgurl": "../../images/cp_1.jpg"
         },
         {
           "id": 2,
           "imgurl": "../../images/ad1.jpg"
         },
         {
           "id": 3,
           "imgurl": "../../images/ad2.jpg"
         }
       ]
     }; 
     that.setData({
       lunboData: data.datas
     }),
     this.data.buttons[0].checked = true;
     this.setData({
       buttons: this.data.buttons,
     })
     var that=this;
     var idt=this.data.idt=false;
     that.setData({
       idt:idt,
     })
     /*if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }*/
   wx.getUserInfo({
     complete: (res) => {
       console.log(res.detail);
       app.globalData.userInfo = res.detail;
     },
     
   })
   },
   photo:function(){
    wx.navigateTo({
      url: '../../pages/photo/photo'
    })
   },
   psv:function(){
    wx.navigateTo({
      url: '../../pages/psv/psv?name='+this.data.name+'&louceng='+this.data.currentLouceng+'&loc='+this.data.location,
    })
   },
   /*获取用户信息*/
   getUserProfile:function(){
    wx.getUserProfile({
      desc:'完善资料',
      success:(res) =>{
       console.log(res.userInfo);
       this.setData({
         userInfo:res.userInfo
       });
       var app = getApp();
       app.globalData.userInfo = res.userInfo;
      },
    })
   },
   /*图片预览*/
   clickImg:function(e){
     var imgurl_loc=this.data.imgurl_loc
     wx.previewImage({
      urls: [imgurl_loc], //需要预览的图片http链接列表，注意是数组
      current: '', // 当前显示图片的http链接，默认是第一个
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
   },
   Active:function(){
     this.setData({IsActive:true})
   },
   swichNav: function (e) {

    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
   bindChange: function (e) {
    var that = this;
    that.setData({ currentTab: e.detail.current });

  },
   changeInfo:function(){
    this.setData({isDisabled:true});
    this.setData({text:'已领取'});
    this.setData({btn:'btn2'})
   },
  Disable:function(){
    this.setData({button_state:true})
  },
  isDisabled(){
    this.setData({isChecked:true})
  },

  onchange(event) {
    wx.showToast({
      title: `切换到标签 ${event.detail.name}`,
      icon: 'none',
    });
  },
  onChange(event) {
    this.setData({
      activeNames: event.detail,
    });
  },
  tels: function () {
    wx.makePhoneCall({
        phoneNumber: '18042543600',
        success: function () {
            console.log("拨打电话成功！")
        },
        fail: function () {
            console.log("拨打电话失败！")
        }
    })
  },
  onClick(event) {
    this.setData({ showShare: true });
  },

  onSelect(event) {
    Toast(event.detail.name);
    this.onClose();
  },
  showDialog1(){
    this.setData({
      show1:true
    })
  },
  showDialog2(){
    if(this.data.IsActive==false){
      wx.showToast({
        title: '请先选择车位',
      })
    }
    else    { this.setData({
      show2:true
    })
  }
  },
  getUserInfo(event) {
    console.log(event.detail);
    this.setData({
      touxiang:e.detail.userInfo.avatarUrl
    });
    app.globalData.touxiang = this.data.touxiang;
  },

  onClose() {
    this.setData({ show1: false });
    this.setData({ showShare: false });
  },
  onidt:function(){
    wx.navigateTo({
      url: '../../pages/name/name',
    })
  },
  onVerify(){
    var that = this;
    if(app.globalData.user.state==0){
      that.setData({
        show2:true,
      })
    }
    else{
    var currentId = that.data.currentCarport
    var carport = JSON.stringify(that.data.carports[currentId]);
    var name = JSON.stringify(that.data.name);
    console.log(carport);
    app.globalData.carport = JSON.parse(carport);
    console.log(app.globalData.carport);
    wx.navigateTo({
      url: '../../pages/pay/pay',
    })
  }
  },
  Disable(){
    this.setData({
      disabled:true
    })
  },
  cp_details(){
    wx.navigateTo({
      url: '../../pages/cp_details/cp_details',
    })
  },
  details_msg:function(){
    wx.navigateTo({
      url: '../../pages/details_msg/details_msg',
    })
  },

cp_select(){
  this.setData({Selected:true});
},
handlePay(){
  wx.navigateTo({
    url: '../../pages/pay/pay',
  })
},
Change:function(){
  this.setData({bg:false})
},
showMap() {
  //使用在腾讯位置服务申请的key（必填）
  const key = "TPYBZ-RTE3V-H4MPY-UKFKI-P5GYJ-VNBVQ"; 
  //调用插件的app的名称（必填）
  const referer = "云购车位"; 
  const location = JSON.stringify({
    longitude:121.6151152,
    latitude:29.9188405
  })
  wx.navigateTo({
      url: 'plugin://chooseLocation/index?key=' + key + '&referer=' + referer + '&location=' + location
  });
},

})