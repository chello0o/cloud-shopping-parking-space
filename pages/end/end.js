// pages/end/end.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rate:5,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

 onChange:function(event){
  wx.showToast({
    title: '感谢您的评分',
    icon: 'icons/duigou.png',
    success: (res) => {},
  })
  console.log(this.data.rate+event.detail);
  this.setData({
    rate:event.detail
  })
 },
 label:function(e){
       console.log(e)
       var that = this;
       that.setData({
     attitude:!e.currentTarget.dataset.index
       })
     },
      label1: function (e) {
       console.log(e)    
       var that = this;
       that.setData({
         time: !e.currentTarget.dataset.index
       })
     },
      label2: function (e) {
        console.log(e)
        var that = this;
        that.setData({
          efficiency: !e.currentTarget.dataset.index
        })
      },
       label3: function (e) {
        console.log(e)
        var that = this;
        that.setData({
          environment: !e.currentTarget.dataset.index
        })
      },
       label4: function (e) {
         console.log(e)
         var that = this;
         that.setData({
           professional: !e.currentTarget.dataset.index
         })
       },
       // 留言
       //字数限制  
     inputs: function (e) {
         // 获取输入框的内容
         var value = e.detail.value;
         // 获取输入框内容的长度
         var len = parseInt(value.length);
         //最多字数限制
         if (len > this.data.max) return;
         // 当输入框内容的长度大于最大长度限制（max)时，终止setData()的执行
         this.setData({
           currentWordNumber: len //当前字数  
         });
       },
       // 图片
       choose: function (e) {//这里是选取图片的方法
       var that = this;
       var pics = that.data.pics;
       wx.chooseImage({
        count: 9, // 最多可以选择的图片张数，默认9
        sizeType: ['original', 'compressed'], // original 原图，compressed 压缩图，默认二者都有
        sourceType: ['album', 'camera'], // album 从相册选图，camera 使用相机，默认二者都有
        success: function (res) {
  
          var imgsrc = res.tempFilePaths;
          pics = pics.concat(imgsrc);
          console.log(pics);
          // console.log(imgsrc);
          that.setData({
            pics: pics,
            // console.log(pics),
          });
        },
      })
  
    },
    uploadimg: function () {//这里触发图片上传的方法
      var pics = this.data.pics;
      console.log(pics);
      app.uploadimg({
        url: 'https://........',//这里是你图片上传的接口
        path: pics//这里是选取的图片的地址数组
      });
    },
    onLoad: function (options) {
  
    },
    // 删除图片
    deleteImg: function (e) {
      var pics = this.data.pics;
      var index = e.currentTarget.dataset.index;
      pics.splice(index, 1);
      this.setData({
        pics: pics
      });
    },
    // 预览图片
    previewImg: function (e) {
      //获取当前图片的下标
      var index = e.currentTarget.dataset.index;
      //所有图片
      var pics = this.data.pics;
      wx.previewImage({
        //当前显示图片
        current: pics[index],
        //所有图片
        urls: pics
      })
    },
    submit:function(){
      wx.showToast({
        title: '感谢您的评价',
        icon: 'icons/duigou.png',
        success: (res) => {
          setTimeout(() => {
            wx.switchTab({
              url: '/pages/index/index',
            })
          }, 1000);
        },
      })
    },
})