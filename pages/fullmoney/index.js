// pages/fullmoney/index.js
var app = getApp();
Page({
    data: {
      radio: '3',
      steps: [
        {
          text: '步骤一',
          desc: '信息审核',
          inactiveIcon: 'arrow',
          activeIcon: 'cirle',
        },
        {
          text: '步骤二',
          desc: '支付定金',
          inactiveIcon: 'arrow',
          activeIcon: 'cirle',
        },
        {
          text: '步骤三',
          desc: '支付全款',
          inactiveIcon: 'success',
          activeIcon: 'success',
        },
        {
          text: '步骤四',
          desc: '寄送合同',
          inactiveIcon: 'arrow',
          activeIcon: 'arrow', 
        },
      ],
    },
  
    onChange(event) {
      this.setData({
        radio: event.detail,
      });
    },

    paysuccess:function(){
      wx.navigateTo({
        url: '../../pages/paysuccess/index',
      })
      app.globalData.user.state = 5
      wx.request({
        url: 'http://112.124.106.13:8000/prod-api/order-weikuan',
        data: {
          id:app.globalData.orderId,
          fangshi:1,
        },
        fail: (err) => {
          console.log("failed");
        },
        header: {"Content-type":"application/x-www-form-urlencoded"},
        method: 'POST',
        success: (res) => {
          console.log("success");
        },
      })
    },

    pay_tail:function(){
      wx.navigateTo({
        url: '../../pages/pay_tail/pay_tail',
      })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
   
})