// pages/message/index.js
import wxbarcode from 'wxbarcode'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        ipname:"翻斗花园",
        card1:'',
        carports_A:[
            {id:0,text:'车位A-23',locked:false,aera:'13m²',price:'¥165,000'},
            {id:1,text:'车位A-24',locked:false,aera:'13m²',price:'¥165,000'},
            {id:2,text:'车位A-25',locked:false,aera:'13m²',price:'¥165,000'},
            {id:3,text:'车位A-26',locked:false,aera:'13m²',price:'¥165,000'},
            {id:4,text:'车位A-27',locked:false,aera:'13m²',price:'¥165,000'},
            {id:5,text:'车位A-28',locked:false,aera:'13m²',price:'¥165,000'},
            {id:6,text:'车位A-29',locked:false,aera:'13m²',price:'¥165,000'},
            {id:7,text:'车位A-30',locked:false,aera:'13m²',price:'¥165,000'},
            {id:8,text:'车位A-31',locked:false,aera:'13m²',price:'¥165,000'},
            {id:9,text:'车位A-32',locked:false,aera:'13m²',price:'¥165,000'},
            {id:10,text:'车位A-33',locked:false,aera:'13m²',price:'¥165,000'},
            {id:11,text:'车位A-34',locked:false,aera:'13m²',price:'¥165,000'},
          ],
          building:[],    
          url:'翻斗花园',
          ID:'1',
          psv:'',
          id:'',
          userName:'kkjjzz',
          telenumber:'12345432'
    },

    /**
     * 生命周期函数--监听页面加载
     */
    request:function(){
        var that=this;
        var url=encodeURI(this.data.url);
        var Url=url;
        var res;
        var id=this.data.ID;
        that.setData({
            Url:url,
        })
        wx.request({
            url: 'http://112.124.106.13:8000/prod-api/carport-photo?id=1',
            data: { 

            },
            dataType: 'JSON',
            header: {'content-type':'application/json'},
            method: 'GET',
          success: (res) => {
        that.setData({
            res:res
        });
        this.setData({psv:res.data})
        console.log(res.data);
        console.log("success")
          },
          fail:(err)=>{
            console.log("fail");
            console.log(url);
            console.log(Url);
        }
        })
        var that = this;
       var article = '<div></div>'
        wxParse.wxParse('article','html',article,that,0);
    },
    onLoad: function (options) {
      wxbarcode.qrcode('qrcode', 'www.baidu.com', 300, 300);
       var that=this;
         wx.request({
           url: 'http://112.124.106.13:8000/prod-api/loupan',
           data: { 
               ipname:this.data.ipname
           },
           dataType: 'JSON',
           header: {'content-type':'application/json'},
           method: 'GET',
           success: (res) => {
              that.setData({
                 list:res.data
              });
              console.log(res.data);
              var array = JSON.parse(res.data);
              this.setData({
                    building:array
              })
              console.log(this.data.building); 
           },
           fail:(err) =>{
               console.log('fail');
           }
         })
    },
    getUserProfile:function(){
     wx.getUserProfile({
       desc:'完善资料',
       success:(res) =>{
        console.log(res.userInfo);
       },
     })
    },
    uploadImage:function(){
      wx.chooseImage({
        count: 1,
        fail: (err) => {
          console.log("failed");
        },
        sizeType: ["original","compressed"],
        sourceType: ["album","camera"],
        success: (res) => {
          console.log("选择成功！");
          var tempFilePath = res.tempFilePaths[0]
          wx.uploadFile({
            filePath: tempFilePath,
            name: 'file',
            url: "http://112.124.106.13:8000/prod-api/post-photo",
            complete: (result) => {
              console.log("complete");
            },
            fail: (err) => {
              console.log("Error!");
            },
            formdata:{
              file:"file"
            },
            header: {"Content-Type": "multipart/form-data" },
            success: (res) => {
              console.log("success");
              console.log(res.data);
            },
          })
        },
      })
    },
    fullMoney:function(){
      wx.navigateTo({
        url: '../../pages/fullmoney/index',
      })
    },
    idRequest:function(){
      wx.request({
        url: 'http://112.124.106.13:8000/prod-api/user',
        data: {
          name:this.data.userName,
          id:0,
          telenumber:this.data.telenumber,
          shenfen:null,
          address:null,

        },
        fail: (err) => {
          console.log("failed");
        },
        header: {"content-Type": "application/x-www-form-urlencoded"},
        method:'POST',
        success: (res) => {
          console.log("success");
        },
      })
    },
    telRequest:function(){
      wx.request({
        url: 'http://112.124.106.13:8000/prod-api/user-id?tel=13993130944',
        fail: (err) => {console.log("failed");},
        header: {"content-type":"application/json"},
        method: 'GET',
        success: (res) => {
          console.log("success");
          console.log(res.data);
          this.setData({
            id:res.data,
          });
          console.log(this.data.id);
        },
      })
    },
    orderRequest:function(){
      wx.request({
        url: 'http://112.124.106.13:8000/prod-api/order-update',
        data:{
        id:1,
        carport:31,
        state:1,
        shoufu:500,
        yongtu:'自用',
        fangshi:1,
        },
        fail: (err) => {
          console.log("failed");
        },
        header: { "content-Type": "application/x-www-form-urlencoded"},
        method: 'POST',
        success: (res) => {
          console.log("success");
        },
      })
    },
    hostRequest:function(){
      var that = this;
      wx.request({
        url: 'http://112.124.106.13:8000/prod-api/order',
        data: {
          id:0,
          user:1,
          state:1,
          ad_zhuang:0,
          ad_danyuan:0,
          ad_fang:0,
          benren:1,
          carport:0,
        },
        fail: (err) => {
          console.log("failed");
        },
        header: { "Content-Type": "application/x-www-form-urlencoded"},
        method: 'POST',
        success: (res) => {
          console.log("success");
        },
      })
      wx.showToast({
        title: '信息已提交,请耐心等待',
      })
      that.setData({
        renzheng:true
      });
      setTimeout(() => {
        wx.switchTab({
          url: '../../pages/index/index',
        })
      }, 5000);
      },
    handleContact:function(event){
      console.log(event.detail.path);
      console.log(event.detail.query);
    },
    name:function(){
      wx.navigateTo({
        url: '../../pages/host/host',
      })
    }

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    //发请求的函数
 
})