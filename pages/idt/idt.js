// pages/idt_p/idt_p.js
var app = getApp();

  
Page({

  /**
   * 页面的初始数据
   */
  data: {
    usr:[{
      usrName:'',usrId:'',usrTel:'',
    },],
    error:false,
    err_usrName:false,
    err_idt_num:false,
    err_tel_num:false,
    err_hostName:false,
    err_room_num:false,
    err_host_idt:false,
    source:' ',
    source2:'',
    user_name:'',
    idt_num:'',
    tel_num:'',
    hostName:'',
    room:[],
    location:'',
    loc:'',
    louceng:'',
    title:'',
    idURL:'',
    host_idt:'',
    show: false,
    multiArray: [['1号楼', '2号楼','3号楼'], ['一楼', '二楼', '三楼', '四楼', '五楼','六楼'], ['1101', '1102','1103']],
    multiIndex: [0, 0, 0],
    tempFilePaths: [],
    tempFilePaths2:[],
    carport:[],
    access_token:'24.bbc8f2cdd6196313160650ac697e47e6.2592000.1632644008.282335-24761479',
    tempName:'',
    tempNum:'',
    idtSrc:'',
    houseSrc:[],
  },
  uploadImage:function(){
    let that = this;
    wx.chooseImage({
      count: 9, // 默认最多9张图片，可自行更改
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: (res) => {
        wx.showToast({
          title: '正在上传...',
          icon: 'loading',
          mask: true,
          duration: 1000
        })
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        let tempFilePath = res.tempFilePaths;
        that.setData({
          tempFilePaths: tempFilePath
        })
        var tempFilePaths = res.tempFilePaths[0]
        that.setData({
          idURL:tempFilePaths
        })
        console.log("Choose imgae success!");
        console.log(tempFilePaths);
        wx.uploadFile({
          filePath: this.data.idURL,
          name: 'file',
          url: "http://112.124.106.13:8000/prod-api/post-photo",
          fail: (err) => {
            console.log("Error!");
          },
          formdata:{
            file:"file"
          },
          header: {"Content-Type": "multipart/form-data" },
          success: (res) => {
            console.log("success");
            this.setData({
              idtSrc:res.data
            })
            console.log(this.data.idtSrc);
            app.globalData.idt_card = this.data.idtSrc
          },
        })
        wx.getFileSystemManager().readFile({
          filePath:this.data.idURL,
          encoding:'base64',
          success:(res) =>{
            //console.log(res.data);  //图片Base64
            that.setData({
              base64:res.data
            });
            wx.request({
              url: 'https://aip.baidubce.com/rest/2.0/ocr/v1/idcard?access_token='+that.data.access_token,
              data: {
                image:that.data.base64,
                id_card_side:'front',
                image_type:'BASE64',
                detect_photo:'true'
              },
              header: {'content-type':'application/x-www-form-urlencoded'},
              method: 'POST',
              success: (res) => {          //OCR识别Success
                console.log(res.data);
                var array = res.data;
                console.log(res.data.words_result.姓名.words);
                that.setData({
                 user_name:res.data.words_result.姓名.words,
                 idt_num:res.data.words_result.公民身份号码.words,
                })
                console.log(this.data.user_name);
                //console.log("头像base64:"+res.data.photo);
              },
              fail:(err) =>{
                console.log("failed");
              }
            })
          }
          
        })
            
      },
    })
  },
  upLoadOneByOne(url,imagePaths,successUp,failUp,count,length){
    var that = this;
    wx.uploadFile({
      url: url,
      filePath: imagePaths[count],
      name: 'file',
      fail: (err) => {
        console.log("failed");
        failUp++;
      },
      success: (res) => {
        successUp++;
       console.log(res.data);
       this.setData({
         ['houseSrc['+count+']']:res.data,
       });
       console.log(that.data.houseSrc);
      },
      complete:function(e){
        count++;
        if(count == length ){
          wx.showToast({
            title: '上传成功！',
            icon:'icons/duigou.png',
          })
        }
        else{
          that.upLoadOneByOne(url,imagePaths,successUp,failUp,count,length)
        }
      },

    })

  },
  uploadImage2:function(){
    var that = this;
    wx.chooseImage({
      count: 9, // 默认最多9张图片，可自行更改
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: (res) => {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths;
        that.setData({
          tempFilePaths2: tempFilePaths
        })
        console.log("Choose imgae success!");
        console.log(tempFilePaths);
        var url = 'http://112.124.106.13:8000/prod-api/post-photo'
        var successUp = 0;
        var failUp = 0;
        var count = 0;
        var length = tempFilePaths.length;
        var imagePaths = that.data.tempFilePaths2;
        that.upLoadOneByOne(url,imagePaths,successUp,failUp,count,length);
        
           /*wx.uploadFile({
              url: 'http://112.124.106.13:8000/prod-api/post-photo',
              filePath: tempFilePaths[i],
              name: 'file',
              fail: (err) => {
                console.log("failed");
              },
              success: (res) => {
               console.log(res.data);
               tempList.push(res.data)
               console.log(tempList);
              },
            })*/
          console.log(this.data.houseSrc);
      },
    })
    
  },
  
  //预览图片

  PreviewImg: function (e) {

    let index = e.target.dataset.index;

    let that = this;

    //console.log(that.data.tempFilePaths[index]);

    //console.log(that.data.tempFilePaths);

    wx.previewImage({

      current: that.data.tempFilePaths[index],

      urls: that.data.tempFilePaths,

    })

  },
  PreviewImg2: function (e) {

    let index = e.target.dataset.index;

    let that = this;

    //console.log(that.data.tempFilePaths[index]);

    //console.log(that.data.tempFilePaths);

    wx.previewImage({

      current: that.data.tempFilePaths2[index],

      urls: that.data.tempFilePaths2,

    })

  },
  DeleteImg: function (e) {
    var that = this;
    var tempFilePaths = that.data.tempFilePaths;
    var index = e.currentTarget.dataset.index;//获取当前长按图片下标
    wx.showModal({
      title: '提示',
      content: '确定要删除此图片吗？',
      success: function (res) {
        if (res.confirm) {
          //console.log('点击确定了');
          tempFilePaths.splice(index, 1);
        } else if (res.cancel) {
          //console.log('点击取消了');
          return false;
        }
        that.setData({
          tempFilePaths
        });
      }
    })
  },
  DeleteImg2: function (e) {
    var that = this;
    var tempFilePaths2 = that.data.tempFilePaths2;
    var index = e.currentTarget.dataset.index;//获取当前长按图片下标
    wx.showModal({
      title: '提示',
      content: '确定要删除此图片吗？',
      success: function (res) {
        if (res.confirm) {
          //console.log('点击确定了');
          tempFilePaths2.splice(index, 1);
        } else if (res.cancel) {
          //console.log('点击取消了');
          return false;
        }
        that.setData({
          tempFilePaths2
        });
      }
    })
  },

 
  bindMultiPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      multiIndex: e.detail.value,
      room:e.detail.value
    })
    console.log(this.data.room[0]);
  },
  bindMultiPickerColumnChange: function (e) {
    console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    var data = {
      multiArray: this.data.multiArray,
      multiIndex: this.data.multiIndex
    };
    data.multiIndex[e.detail.column] = e.detail.value;
    console.log(e.detail.column);
    switch (e.detail.column) {
      case 0:
        switch (data.multiIndex[0]) {
          case 0:
            data.multiArray[1] = ['一楼', '二楼', '三楼', '四楼', '五楼','六楼'];
            data.multiArray[2] = ['1101', '1102','1103'];
            break;
          case 1:
            data.multiArray[1] = ['一楼', '二楼', '三楼', '四楼', '五楼','六楼'];
            data.multiArray[2] = ['2101', '2102','2103'];
            break;
          case 2:
            data.multiArray[1] = ['一楼', '二楼', '三楼', '四楼', '五楼','六楼'];
            data.multiArray[2] = ['3101', '3102','3103'];
            break;     
        }
        data.multiIndex[1] = 0;
        data.multiIndex[2] = 0;
        break;
      case 1:
        switch (data.multiIndex[0]) {
          case 0:
            switch (data.multiIndex[1]) {
              case 0:
                data.multiArray[2] = ['1101', '1102','1103'];
                break;
              case 1:
                data.multiArray[2] = ['1201','1202','1203'];
                break;
              case 2:
                data.multiArray[2] = ['1301','1302','1303'];
                break;
              case 3:
                data.multiArray[2] = ['1401','1402','1403'];
                break;
              case 4:
                data.multiArray[2] = ['1501','1502','1503'];
                break;
              case 5:
                data.multiArray[2] = ['1601','1602','1603'];
                break;
            }
            break;
            case 1:
            switch (data.multiIndex[1]) {
              case 0:
                data.multiArray[2] = ['2101', '2102','2103'];
                break;
              case 1:
                data.multiArray[2] = ['2201','2202','2203'];
                break;
              case 2:
                data.multiArray[2] = ['2301','2302','2303'];
                break;
              case 3:
                data.multiArray[2] = ['2401','2402','2403'];
                break;
              case 4:
                data.multiArray[2] = ['2501','2502','2503'];
                break;
              case 5:
                data.multiArray[2] = ['2601','2602','2603'];
                break;
            }
            break;
            case 2:
            switch (data.multiIndex[1]) {
              case 0:
                data.multiArray[2] = ['3101', '3102','3103'];
                break;
              case 1:
                data.multiArray[2] = ['3201','3202','3203'];
                break;
              case 2:
                data.multiArray[2] = ['3301','3302','3303'];
                break;
              case 3:
                data.multiArray[2] = ['3401','3402','3403'];
                break;
              case 4:
                data.multiArray[2] = ['3501','3502','3503'];
                break;
              case 5:
                data.multiArray[2] = ['3601','3602','3603'];
                break;
            }
            break;
        }
        data.multiIndex[2] = 0;
        console.log(data.multiIndex);
        break;
    }
    this.setData(data);
  },
  onLoad:function(options){
    var carport = JSON.parse(options.carport);
    var title = options.title;
    var loc = options.loc;
    var louceng = options.louceng;
    this.setData({
      carport:carport,
      title:title,
      loc:loc,
      louceng:louceng,

    });
    console.log(carport);
  },
  getUsrName:function(e){
    this.setData({usrname:e.detail});
  },
  getIdt_num:function(e){
    this.setData({idt_num:e.detail});
  },
  getTel_num:function(e){
    this.setData({tel_num:e.detail});
  },
  getHost_idt:function(e){
    this.setData({host_idt:e.detail});
  },
  getHostName:function(e){
    this.setData({hostName:e.detail});
  },
  getRoom_num:function(e){
    this.setData({room_num:e.detail});
  },
  
none_text:function(event){
  if(this.data.usrname==''){
    this.setData({err_usrName:true});
  }
  else{
    this.setData({err_usrName:false});
  }
  if(this.data.idt_num.length!=18){
    this.setData({err_idt_num:true});
  }
  else{
    this.setData({err_idt_num:false});
  }
  if(this.data.host_idt.length!=18){
    this.setData({err_host_idt:true});
  }
  else{
    this.setData({err_host_idt:false});
  }
  if(this.data.tel_num.length!=11)
  this.setData({err_tel_num:true});
  else{
    this.setData({err_tel_num:false});
  }
  if(this.data.hostName=='')
  this.setData({err_hostName:true});
  else{
    this.setData({err_hostName:false});
  }
 
  if(this.data.err_usrName==false&&this.data.err_tel_num==false&&this.data.err_tel_num==false&&this.data.err_hostName==false&&this.data.err_room_num==false&&this.data.err_host_idt==false){
  console.log(event.detail)
  
  
    /*wx.navigateTo({
    url: '../../pages/pay/pay',
  })*/
}
},


/*print:function(){
  console.log(this.data.usrname);
  console.log(this.data.idt_num);
  console.log(this.data.tel_num);
  console.log(this.data.hostName);
  console.log(this.data.room_num);
},*/
showDialog() {
  this.setData({ show: true });
},

onClose() {
  this.setData({ show: false });
},
uploading:function(){
  var that = this;
  wx.chooseImage({  //从本地相册选择图片或使用相机拍照
    count: 3, // 默认9
    sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
    sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有

    success:function(res){
      //console.log(res)
     //前台显示
      that.setData({
        source: res.tempFilePaths
      })

      // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
      var tempFilePaths = res.tempFilePaths
       wx.uploadFile({
        url: 'http://www.website.com/home/api/uploadimg',
        filePath: tempFilePaths[0],
        name: 'file',
        
        success:function(res){
          //打印
          console.log(res.data)
        }
      })
      
    }
  })
  data.multiIndex[e.detail.column] = e.detail.value;

  this.setData(data);
},
usrNameInput:function(event){
this.setData({
  user_name:event.detail,
})
},
usrIdInput:function(event){
  this.setData({
    idt_num:event.detail
  });
},
usrTelInput:function(event){
  this.setData({
    tel_num:event.detail
  });
},
usrAdrsInput:function(event){
},
usrRequest:function(){

  var that = this;
  var carport = JSON.stringify(this.data.carport);
  var title = JSON.stringify(this.data.title);
  var user_name = JSON.stringify(this.data.user_name);
  var idt_num = JSON.stringify(this.data.idt_num);
  var tel_num = JSON.stringify(this.data.tel_num);
  var loc = JSON.stringify(this.data.loc);
  var louceng = JSON.stringify(this.data.louceng);
  console.log("姓名:"+this.data.user_name);
  console.log("身份证号:"+this.data.idt_num);
  console.log("电话号码："+this.data.tel_num);
  console.log('楼盘名：'+this.data.title);
  app.globalData.user.user_name = this.data.user_name;
  app.globalData.user.idt_num = this.data.idt_num;
  app.globalData.user.tel_num = this.data.tel_num;
  app.globalData.user.room = this.data.room;
  console.log(app.globalData.build);
  console.log(app.globalData.carport);
  console.log(app.globalData.user);
  wx.request({
    url: 'http://112.124.106.13:8000/prod-api/order',
    method:"POST",
    header:   {'content-Type':'application/text'},
    data: {
      id:10000,
      user:2,        
      state:1,
      ad_zhuang:1,
      shenfen:'1234',
      ad_danyuan:3,
      ad_fang:2,
      goufang:'//1.jpg',
    },

    success: (res) => {
      console.log("success");
      console.log(res.data);
    },
    fail: (err) => {
      console.log("error");
    }
  })
  app.globalData.house = this.data.houseSrc;
  console.log(app.globalData.house);
  wx.navigateTo({
    url: '../../pages/audit/audit?carport='+carport+'&user_name='+user_name+'&idt_num='+idt_num+'&tel_num='+tel_num+'&room='+this.data.room+'&title='+this.data.title+'&loc='+this.data.loc+'&louceng='+this.data.louceng,
  })
},

})
