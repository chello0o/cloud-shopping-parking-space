// pages/pay_tail/pay_tail.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  payRequest:function(){
    app.globalData.user.state = 7
    wx.request({
      url: 'http://112.124.106.13:8000/prod-api/order-weikuan',
      data: {
        id:app.globalData.orderId,
        fangshi:0,
      },
      fail: (err) => {
        console.log("failed");
      },
      header: {"content-type":"application/x-www-form-urlencoded"},
      method: 'POST',
      success: (res) => {
        console.log("success");
      },
    })
    wx.showToast({
      title: '申请提交成功\n即将返回主页',
    })
    setTimeout(() => {
      wx.switchTab({
        url: '../../pages/index/index',
      })
    }, 2000);
    
  }
})