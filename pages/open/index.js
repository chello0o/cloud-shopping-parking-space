
Page({

    /**
     * 页面的初始数据
     */
    data: {
      url:"http://112.124.106.13:8000/prod-api/prod-api/loupan-state?state=2",
      columns:[],
        value: '',
        actve:1,
        option1: [
          { text: '全部状态', value:0 },
          { text: '未开始认筹', value:1 },
          { text: '已开始认筹', value:2 },
          { text: '未开盘', value:3 },
          { text: '已开盘', value:4 },
        ],        
        option2: [
          { text: '默认排序', value: 'a' },
          { text: '由低到高', value: 'b' },
          { text: '由高到低', value: 'c' },
        ],
        option3:[
          { text:'全部区域',value :'x'},
          { text:'杭州',value :'z'},
          { text:'宁波',value :'y'},
        ],
        option4:[
          {text:'默认排序',value :5 },
          {text:'由低到高',value :6 },
          {text:'由高到低',value :7 },
        ],
        value1: 0,
        value2: 'a',
        value3:'x',
        value4:5,
        title1:'开盘状态',
        title2:'价格',
        title3:'区域',
        title4:'面积',
        carport_item:[
          {
            tag:'已开盘',
          price:'160,000',
          origin_price:'200,000',
          desc:'类型：产权      面积：13m²',
          title:'保利紫山花苑二期',
          thumb:'https://wx2.sinaimg.cn/mw690/006qbn5yly1gslaxwto18j30d609b74t.jpg'},
        {
    
        }
        ],
        building:[],
    },
    
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad:function(){
      var that = this;
      wx.request({
        url: "http://112.124.106.13:8000/prod-api/prod-api/loupan",
        dataType: 'JSON',
        fail: () => {
          console.log("failed");
        },
        header:  {'content-type':'application/json'},
        method: 'GET',
        success: (res) => {
         console.log(res.data);
         var array = JSON.parse(res.data);
         that.setData({
           building:array
         });
         console.log(that.data.building);
         for(var i=0;i<array.length;i++){
           that.setData({
            ["columns["+i+"]"]:array[i].name
           })
           console.log(that.data.columns);
          
         }
        
        },
      })
    },
    onChange(e) {
      this.setData({
        value: e.detail,
      });
    },
    onSearch() {
      Toast('搜索' + this.data.value);
    },
    onClick() {
      Toast('搜索' + this.data.value);
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
    },
    onChange(e) {
        this.setData({
          value: e.detail,
        });
      },
      onChange(event) {
        wx.showToast({
          title: `切换到标签 ${event.detail.name}`,
          icon: 'none',
        });
      },
      onClick(event) {
        wx.showToast({
          title: `点击标签 ${event.detail.name}`,
          icon: 'none',
        });
      },
      onSearch() {
        Toast('搜索' + this.data.value);
      },
      onClick() {
        Toast('搜索' + this.data.value);
      },
      //点击置顶
      onPageScroll: function (e){
        console.log(e);
        if (e.scrollTop > 100) {
          this.setData({
            floorstatus: true
          });
        } else {
          this.setData({
            floorstatus: false
          });
        }
      },
      goTop: function () {  
        if (wx.pageScrollTo) {
          wx.pageScrollTo({
            scrollTop: 0
          })
        } else {
          wx.showModal({
            title: '提示',
            content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
          })
        }
      },
    });