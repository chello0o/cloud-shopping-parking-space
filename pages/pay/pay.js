// pages/pay_p/pay_p.js
var app = getApp();

  
Page({

  /**
   * 页面的初始数据
   */
  data: {
    carport:[],
    build:{
    },
    user:{},
    userInfo:{},
    host:{},
    userId:'',
    title:'',
    time: 2 * 59 * 60 * 1000,
    timeData: {},
    loc:'',
    louceng:'',
    showDialog:false,
   
  }, 
  onChange(e) {
    this.setData({
      timeData: e.detail,
    });
  },
 onLoad:function(options){
   var that = this;
  var carport =  app.globalData.carport;
  var build = app.globalData.build;
  var user = app.globalData.user;
  var host = app.globalData.host;
  var userId = app.globalData.userId;
  that.setData({
    carport:carport,
    build:build,
    user:user,
    host:host,
    userId:userId,
  });
  console.log(carport);
  console.log(app.globalData);
  console.log(this.data);
 }, 
  payRequest:function(){
    var that = this;
    /*wx.request({
      url: 'http://112.124.106.13:8000/oprod-api/rder?id='+that.data.userId,
      header:{'content-type':'application/json'},
      method: 'GET',
      data:{
      },
      fail:(err) =>{
        console.log("failed");
      },
      success: (res) => {
        that.setData({
          active:res.data.state,
          user:user,
          host:host,
        })
      app.globalData.state = that.data.active
      },
    })*/
    var state = app.globalData.user.state;
    if(state==0){
      this.setData({
        showDialog:true,
      })
    }
    wx.request({
      url: 'http://112.124.106.13:8000/prod-api/order-update',
      method: "POST",
      header:{"content-Type": "application/x-www-form-urlencoded"
    },
      data: {
        id:app.globalData.orderId,
        carport:that.data.carport.id,
        state:that.data.carport.state,
        shoufu:5000,//that.data.carport.renchou,
        yongtu:'自用',
        fangshi:1,
      },
      success: (res) => {
        console.log("success");
        console.log(res);
      },
    })
    if(that.data.user.state!=0)
    wx.showToast({
    title: '支付成功',
    icon: '../../icons/duigou.png',
    duration: 1000,
    success:()=>{
        setTimeout(()=> {
            wx.navigateTo({
              url:'/pages/Qrcode/Qrcode',
            })
        },1000)
    }
  })

  },
  
})